# Tondeuse problem
## Description
// todo : quick description

refer to `./doc/tondeuse-prob.pdf` for detailed description

## Solution 
* clone the package 

    git clone https://gitlab.com/amineal/exercice.git

* install and run the package 

````
	cd ./exercice
    mvn clean install
    java -jar .\target\exercice-0.0.1-SNAPSHOT.jar .\doc\example-input.txt
````
