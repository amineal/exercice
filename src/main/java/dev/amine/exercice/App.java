package dev.amine.exercice;

import dev.amine.exercice.tondeuse.exceptions.ParsingException;

public class App {

	public static void main(String[] args) throws ParsingException {
		if (args.length != 1) {
			System.err.println("No file path param given !");
			System.exit(-1);
		}

		try {
			TondeuseSolution.execute(args[0]);
		} catch (ParsingException e) {
			System.err.println("Error:" + e.getMessage());
			System.exit(-1);
		}
	}
}
