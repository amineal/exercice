package dev.amine.exercice;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import dev.amine.exercice.tondeuse.Tondeuse;
import dev.amine.exercice.tondeuse.exceptions.ParsingException;
import dev.amine.exercice.tondeuse.models.Coordinate;
import dev.amine.exercice.tondeuse.models.Orientation;

public final class TondeuseSolution {
    
    private TondeuseSolution(){}

    /**
     * Executes the solution to the tondeuse's problem
     * 
     * @param inputFile the input's file
     * @throws ParsingException a parsing exception
     */
    public static void execute(String inputFile) throws ParsingException {
        BufferedReader reader = null;
        
        try {
            // load the file's buffer
            reader = new BufferedReader(new FileReader(inputFile));
            
            // read pelouse max coords
            String currentLine = reader.readLine();
            Coordinate maxCoords = parseMaxPelouseCoords(currentLine);
            
            // read tondeuse and its instructions
            while( (currentLine = reader.readLine()) != null ) {
                String[] splited = currentLine.split(" ");

                if (splited.length != 3) {
        			throw new ParsingException("Error during tondeuse info parsing");
        		}
                
                int x = Integer.parseInt(splited[0]);
                int y = Integer.parseInt(splited[1]);
                char o = splited[2].charAt(0);

                Coordinate coords = new Coordinate(x, y);
                Orientation orient = Orientation.getByChar(o);

                Tondeuse tondeuse = Tondeuse.builder()
                                            .coordinate(coords)
                                            .maxCoordinate(maxCoords)
                                            .orientation(orient)
                                            .build();

                tondeuse.executeInstructionsAndPrint(
                    parseTondeuseInstructions(reader.readLine()), 
                    (output) -> System.out.println(output)
                );
            }
        } catch( FileNotFoundException e ) {
            throw new ParsingException("File was not found");
        } catch (IOException e) {
            throw new ParsingException("Error while reading file");
        } finally {
            if ( reader != null ) { 
                try {
                    reader.close();
                } catch (IOException e) {
                    throw new ParsingException("Error while closing reader");
                } 
            }
        }
        
    }

    /**
     * Parses the tondeuse's instructions from a given instruction
     * line 
     * 
     * @param instLine the instruction line
     * @return an array of instructions
     * @throws ParsingException a parsing exception
     */
    private static char[] parseTondeuseInstructions(String instLine) throws ParsingException {
        if( instLine == null ) {
        	throw new ParsingException("Error during instructions parsing");
        }
        return instLine.toCharArray();
    }

    /**
     * Parses the pelouse's max coordinates from a given pelouse Line
     * 
     * @param pelCoordsLine the pelouse's line
     * @return pelouse's max coordinates
     * @throws ParsingException a parsing exception
     */
    private static Coordinate parseMaxPelouseCoords(String pelCoordsLine) throws ParsingException {
        if( pelCoordsLine == null ) {
        	throw new ParsingException("Error during pelouse coordinates parsing");
        }
    	String splited[] = pelCoordsLine.split(" ");
        if (splited.length != 2) {
			throw new ParsingException("Error during pelouse coordinates parsing");
		}
        return new Coordinate(Integer.parseInt(splited[0]), Integer.parseInt(splited[1]));
    }

}