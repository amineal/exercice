package dev.amine.exercice.tondeuse;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import dev.amine.exercice.tondeuse.actions.MoveForwardAction;
import dev.amine.exercice.tondeuse.actions.TAction;
import dev.amine.exercice.tondeuse.actions.TActionRunner;
import dev.amine.exercice.tondeuse.actions.TurnLeftAction;
import dev.amine.exercice.tondeuse.actions.TurnRightAction;
import dev.amine.exercice.tondeuse.exceptions.ParsingException;
import dev.amine.exercice.tondeuse.models.Coordinate;
import dev.amine.exercice.tondeuse.models.Orientation;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * tondeuse's representation 
 * @author Amine
 */
@Builder
public class Tondeuse {

    @Getter @Setter private Coordinate  coordinate;
    @Getter @Setter private Coordinate  maxCoordinate;
    @Getter @Setter private Orientation orientation;
    
    // list of possible instructions
    private final List<Character> possibleInst = Arrays.asList('A', 'D', 'G') ;
    private final TActionRunner   actionRunner = new TActionRunner();

    // mapping betweens possible instructions and their actions.
    private final Map<Character, TAction> instActionMap  = Map.of(
        'G', new TurnLeftAction(this),
        'D', new TurnRightAction(this),
        'A', new MoveForwardAction(this)
    );
    
    public Tondeuse(Coordinate initCoords, Coordinate maxCoords, Orientation initOrient) {
        this.coordinate 	= initCoords;
        this.maxCoordinate 	= maxCoords;
        this.orientation 	= initOrient;
    }

    /**
     * Executes instructions passed in params and prints 
     * output using printer function given in param 
     * 
     * @param instructions list of instructions ('A', 'G' or 'D')
     * @param printer the printer's function
     * @throws ParsingException a parsing exception
     */
    public  void executeInstructionsAndPrint(final String instructions, final IPrinter printer) throws ParsingException {
    	if(instructions == null ) {
    		throw new ParsingException("Bad instructions");
    	}
    	executeInstructionsAndPrint(instructions.toCharArray(), printer);
    }
    
    /**
     * Executes instructions passed in params and prints 
     * output using printer function given in param 
     * 
     * @param instructions list of instructions ('A', 'G' or 'D')
     * @param printer the printer's function
     * @throws ParsingException a parsing exception
     */
    public  void executeInstructionsAndPrint(final char[] instructions, final IPrinter printer) throws ParsingException {
        loadInstructions(instructions);
        executeActions();
        printCoordinatesAndOrientation(printer);
    }

    /**
     * Execute all loaded actions
     */
    private void executeActions() {
        actionRunner.executeAll();
    }

    /**
     * Parses and load instructions 
     * 
     * @param instructions the instructions
     * @throws ParsingException a parsing exception
     */
    private void loadInstructions(final char[] instructions) throws ParsingException {
        for (char c : instructions) {
            validateInstruction(c);
            actionRunner.addAction(instActionMap.get(c));
        }
    }

    /**
     * Validate instruction 
     * 
     * @param instruction the instruction
     * @throws ParsingException a parsing exception
     */
    private void validateInstruction(char instruction) throws ParsingException {
        if ( !possibleInst.contains(instruction) ) {
            throw new ParsingException("Insupported instruction : " + instruction);
        }
    }

    /**
     * Prints the current coordinates and the orientation of 
     * the tondeuse using a given printer's function
     * 
     * @param printer the printer's function
     */
    private  void printCoordinatesAndOrientation(IPrinter printer) {

        String output = new StringBuilder()
                    .append(this.coordinate.getX())
                    .append(" ")
                    .append(this.coordinate.getY())
                    .append(" ")
                    .append(this.orientation.getOneCharName())
                    .toString();

        printer.print(output);
    }

    /**
     * Printer functionnal interface
     */
    public interface IPrinter {
        void print(String output);
    }
    
}
