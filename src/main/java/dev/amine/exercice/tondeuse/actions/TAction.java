package dev.amine.exercice.tondeuse.actions;

import dev.amine.exercice.tondeuse.Tondeuse;
/**
 * Representation of an abstract Tendeuse action
 * @author Amine  
 */
public abstract class TAction {

    protected Tondeuse tondeuse;

    public TAction(Tondeuse Tondeuse) {
        this.tondeuse = Tondeuse;
    }

    public abstract void execute();
}