package dev.amine.exercice.tondeuse.actions;

import java.util.LinkedList;
import java.util.Queue;

/**
 * A tendeuse action runner to host 
 * the actions sequence and utilities 
 * @author Amine
 */
public class TActionRunner {

    private Queue<TAction> actionsQueue = new LinkedList<>();
    
    public void addAction(TAction action) {
        if (action == null ) return;
        actionsQueue.add(action);
    }

    public void executeAll(){
        while (! actionsQueue.isEmpty()) {
            actionsQueue.poll().execute();
        }
    }

}
