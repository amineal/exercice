package dev.amine.exercice.tondeuse.actions;

import dev.amine.exercice.tondeuse.Tondeuse;
import dev.amine.exercice.tondeuse.models.Orientation;

/**
 * Turn left action : rotates the tendeuse
 * 90° to the left
 * @author Amine
 */
public class TurnLeftAction    extends TAction {
    
    public TurnLeftAction(Tondeuse tondeuse) {
        super(tondeuse);
    }
    
    @Override
    public void execute() {
        Orientation currOr = this.tondeuse.getOrientation();
        this.tondeuse.setOrientation(
            // we set the tendeuse's orientation to the previous orientation in the 
            // ordered cyclic list of orientations, by decrementing it 
            // (if we're on the first position, the previous one is the last in array.)
            Orientation.getByIndex(
                (currOr.index != 0) ? 
                            currOr.index - 1 : 
                            Orientation.values().length - 1)
        );        
    }
}