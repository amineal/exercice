package dev.amine.exercice.tondeuse.actions;

import dev.amine.exercice.tondeuse.Tondeuse;
import dev.amine.exercice.tondeuse.models.Orientation;

/**
 * Turn right action : rotates the tendeuse
 * 90° to the right
 * @author Amine
 */
public class TurnRightAction   extends TAction {

    public TurnRightAction(Tondeuse tondeuse) {
        super(tondeuse);
    }

    @Override
    public void execute() {
        Orientation currOr = this.tondeuse.getOrientation();
        this.tondeuse.setOrientation(
            // we set the tendeuse's orientation to the next orientation in the ordered
            // cyclic list of orientations by incrementing it
            Orientation.getByIndex((currOr.index + 1) % Orientation.values().length)
        );
    }

}