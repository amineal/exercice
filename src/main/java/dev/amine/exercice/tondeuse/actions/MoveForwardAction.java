package dev.amine.exercice.tondeuse.actions;

import dev.amine.exercice.tondeuse.Tondeuse;
import dev.amine.exercice.tondeuse.models.Coordinate;
import dev.amine.exercice.tondeuse.models.Orientation;

/**
 * Move forward action: moves the tondeuse
 * in the direction of its orientation
 * @author Amine
 */
public class MoveForwardAction extends TAction {

    public MoveForwardAction(Tondeuse tondeuse) {
        super(tondeuse);
    }

    @Override
    public void execute() {
        Coordinate currCoords = this.tondeuse.getCoordinate();
        Orientation currOrient = this.tondeuse.getOrientation();

        // we increment the tondeuse's coordinates depending on its
        // orientation
		switch(currOrient) {
            
			case NORTH : 
            	if(canMoveToY(currCoords.getY() + 1)) 
            		currCoords.setY(currCoords.getY() + 1); 
            	break;
            
            case EAST  :    
            	if(canMoveToX(currCoords.getX() + 1)) 
            		currCoords.setX(currCoords.getX() + 1); 
            	break;
            
            case SOUTH :    
            	if(canMoveToY(currCoords.getY() - 1)) 
            		currCoords.setY(currCoords.getY() - 1); 
            	break;
            
            case WEST  :    
            	if(canMoveToX(currCoords.getX() - 1)) 
            		currCoords.setX(currCoords.getX() - 1); 
            	break;
            	
            default: break;
        }
    }
        
    /**
     * Verify if next X coord is authorized
     * 
     * @param nextX next X coord
     * @return true if next X coord is authorized, else false 
     */
    private boolean canMoveToX(int nextX) {
    	Coordinate max = this.tondeuse.getMaxCoordinate();
    	return ( 0 <= nextX && nextX <= max.getX() );
    }
    
    /**
     * Verify if next Y coord is authorized
     * 
     * @param nextX next Y coord
     * @return true if next Y coord is authorized, else false 
     */
    private boolean canMoveToY(int nextY) {
    	Coordinate max = this.tondeuse.getMaxCoordinate();
    	return ( 0 <= nextY && nextY <= max.getY() );
    }
}
