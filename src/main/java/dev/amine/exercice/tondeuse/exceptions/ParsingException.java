package dev.amine.exercice.tondeuse.exceptions;

/**
 * A parsing exception
 * @author Amine
 */
public class ParsingException extends Exception {

	private static final long serialVersionUID = 5544856009222633193L;

	public ParsingException(String message) {
        super(message);
    }
}
