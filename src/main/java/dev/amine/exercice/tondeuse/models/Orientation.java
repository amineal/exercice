package dev.amine.exercice.tondeuse.models;

/**
 * An Orientation enum to contain 4 main 
 * directions, ordered in a cyclic manner
 * @author Amine
 */
public enum Orientation {

    NORTH(0), EAST(1), SOUTH(2), WEST(3);

    public int index;

    private Orientation(int index) {
        this.index = index;
    }

    /**
     * Get an enumeration by its index 
     * @param index the index number
     * @return the corresponding enumeration
     */
    public static Orientation getByIndex(int index) {

        for (Orientation o : Orientation.values()) {
            if (o.index == index) {
                return o;
            }
        }

        throw new RuntimeException("Invalid value for orientation index : " + index);
    }

    /**
     * Get an enumeration by it first character
     * @param ch the orientation character
     * @return the corresponding enumeration
     */
    public static Orientation getByChar(char ch) {

        for (Orientation o : Orientation.values()) {
            if (o.getOneCharName() == ch) {
                return o;
            }
        }

        throw new RuntimeException("Invalid value for orientation char : " + ch);
    }

    /**
     * Get a one character name of enum
     * @return one character name
     */
    public char getOneCharName() {
        return getName().charAt(0);
    }

    /**
     * Get enum's full name
     * @return enum's full name
     */
    public String getName() {
        return this.toString();
    }

}
