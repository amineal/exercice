package dev.amine.exercice.tondeuse.models;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Simple 2D coordinate representation
 * @author Amine
 */
@Data
@AllArgsConstructor
public class Coordinate {
    private int x; 
    private int y;
}
