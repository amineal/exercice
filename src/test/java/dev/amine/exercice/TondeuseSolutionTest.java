package dev.amine.exercice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import dev.amine.exercice.tondeuse.exceptions.ParsingException;

public class TondeuseSolutionTest {
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;

	@BeforeEach
	public void setUpStreams() {
	    System.setOut(new PrintStream(outContent));
	}

	@AfterEach
	public void restoreStreams() {
	    System.setOut(originalOut);
	}
	
	@Test
	@DisplayName("execute with the problem's given input should succeed")
	public void executeTestRightCase() throws ParsingException {
		// given
		String lineSepar = System.lineSeparator();
		String inputFile = "src/test/resources/problem-input";
		
		// when 
		TondeuseSolution.execute(inputFile);			

		// then
		assertEquals(
				outContent.toString(), 
				"1 3 N" + 
				lineSepar +
				"5 1 E" +
				lineSepar
				);
	}
	
	@Test
	@DisplayName("execute with many tondeuses should succeed")
	public void executeTestWithManyTondeuses() throws ParsingException {
		// given
		String lineSepar = System.lineSeparator();
		String inputFile = "src/test/resources/many-tondeuses";
		
		// when 
		TondeuseSolution.execute(inputFile);			
		
		// then
		assertEquals(
				outContent.toString(), 
				"1 3 N" + 
				lineSepar +
				"5 1 E" +
				lineSepar +
				"0 1 W" +
				lineSepar
				);
	}	
	
	@Test
	@DisplayName("execute with invalid or absent instructions should throw a parsing exception")
	public void executeTestWithInvalidOrAbsentInstructions() {
		// given
		String inputFile = "src/test/resources/invalid-or-absent-instructions";
		
		// then, when 
		Exception exception = assertThrows(ParsingException.class, () -> {
			TondeuseSolution.execute(inputFile);			
		});			
		
		// then
		assertTrue(exception.getMessage().startsWith("Insupported instruction"));

	}	

	@Test
	@DisplayName("execute with non existing file should throw a parsing exception")
	public void executeTest() throws ParsingException {
		// given
		String inputFile = "src/test/resources/non-existing-file";
		
		// then, when 
		Exception exception = assertThrows(ParsingException.class, () -> {
			TondeuseSolution.execute(inputFile);			
		});
		
		assertEquals(exception.getMessage(), "File was not found");
	}
	
	@Test
	@DisplayName("execute with a file with no instructions should throw a parsing exception")
	public void executeTestWrongInstructions() throws ParsingException {
		// given
		String inputFile = "src/test/resources/tondeuse-no-inst";
		
		// then, when 
		Exception exception = assertThrows(ParsingException.class, () -> {
			TondeuseSolution.execute(inputFile);			
		});
		
		assertEquals(exception.getMessage(), "Error during instructions parsing");
	}
	
	@Test
	@DisplayName("execute with a file with wrong tondeuse info should throw a parsing exception")
	public void executeTestWrongTondeuseInfo() throws ParsingException {
		// given
		String inputFile = "src/test/resources/wrong-tondeuse-info";
		
		// then, when 
		Exception exception = assertThrows(ParsingException.class, () -> {
			TondeuseSolution.execute(inputFile);			
		});
		
		assertEquals(exception.getMessage(), "Error during tondeuse info parsing");
	}
	
	@Test
	@DisplayName("execute with a file with wrong pelouse coords should throw a parsing exception")
	public void executeTestWrongPelouseCoords() throws ParsingException {
		// given
		String inputFile = "src/test/resources/wrong-pelouse-coords";
		
		// then, when 
		Exception exception = assertThrows(ParsingException.class, () -> {
			TondeuseSolution.execute(inputFile);			
		});
		
		assertEquals(exception.getMessage(), "Error during pelouse coordinates parsing");
	}
	
	@Test
	@DisplayName("execute with a file with no pelouse coords info should throw a parsing exception")
	public void executeTestNoPelouseCoords() throws ParsingException {
		// given
		String inputFile = "src/test/resources/no-pelouse-info";
		
		// then, when 
		Exception exception = assertThrows(ParsingException.class, () -> {
			TondeuseSolution.execute(inputFile);			
		});
		
		assertEquals(exception.getMessage(), "Error during pelouse coordinates parsing");
	}
	
}
