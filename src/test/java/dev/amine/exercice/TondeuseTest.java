package dev.amine.exercice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import dev.amine.exercice.tondeuse.Tondeuse;
import dev.amine.exercice.tondeuse.exceptions.ParsingException;
import dev.amine.exercice.tondeuse.models.Coordinate;
import dev.amine.exercice.tondeuse.models.Orientation;

public class TondeuseTest {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;

	@BeforeEach
	public void setUpStreams() {
	    System.setOut(new PrintStream(outContent));
	}

	@AfterEach
	public void restoreStreams() {
	    System.setOut(originalOut);
	}
	
	@Test
	@DisplayName("execute problem's input right case should give right output")
	public void executeInstructionsAndPrintTest() throws ParsingException {
		// given
		Coordinate maxCoords 	= new Coordinate(5,5);
		
		Tondeuse fstTondeuse = new Tondeuse(
				new Coordinate(1,2), 
				maxCoords, 
				Orientation.NORTH
				);
		
		String fstTInstructions = "GAGAGAGAA";
		
		Tondeuse scdTondeuse = new Tondeuse(
				new Coordinate(3,3), 
				maxCoords, 
				Orientation.EAST
				);
		
		String scdTInstructions = "AADAADADDA";
		
		// when 
		fstTondeuse.executeInstructionsAndPrint(
				fstTInstructions, 
				(output) -> System.out.print(output)
				);
		
		// then
	    assertEquals("1 3 N", outContent.toString());

	    outContent.reset();
	    
	    // when
		scdTondeuse.executeInstructionsAndPrint(
				scdTInstructions, 
				(output) -> System.out.print(output)
				);
		
		// then
	    assertEquals("5 1 E", outContent.toString());
		
	}
	
	@Test
	@DisplayName("execute with invalid or absent instructions should throw a parsing exception")
	public void executeInstructionsAndPrintTestwrongInstructions() {
		// given
		Coordinate maxCoords 	= new Coordinate(5,5);
		
		Tondeuse tondeuse = new Tondeuse(
				new Coordinate(1,2), 
				maxCoords, 
				Orientation.NORTH
				);
		
		String instructions = null;
		
		// then, when 
		Exception exception = assertThrows(ParsingException.class, () -> {
			tondeuse.executeInstructionsAndPrint(
					instructions, 
					(output) -> System.out.print(output)
					);		
		});			
		
		// then
		assertTrue(exception.getMessage().startsWith("Bad instructions"));

	}	
}
