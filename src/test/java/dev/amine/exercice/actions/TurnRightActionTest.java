package dev.amine.exercice.actions;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import dev.amine.exercice.tondeuse.Tondeuse;
import dev.amine.exercice.tondeuse.actions.TurnRightAction;
import dev.amine.exercice.tondeuse.models.Coordinate;
import dev.amine.exercice.tondeuse.models.Orientation;

public class TurnRightActionTest {

	/**
	 * 	   NORTH
	 * <WEST * EAST>
	 *	   SOUTH
	 */
	
	private Coordinate maxCoords;
	private Coordinate startCoords;

	@BeforeEach
	public void setUp() {
		this.maxCoords = new Coordinate(3, 3);
		this.startCoords = new Coordinate(0, 0);
	}
	
	@Test()
	@DisplayName("turning right from NORTH should give EAST")
	public void testExecuteFromNorth()   {

		// given
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.NORTH);

		// when 
		new TurnRightAction(tondeuse).execute();
		
		// then
		assertSame(tondeuse.getOrientation(), Orientation.EAST);
	}
	
	@Test()
	@DisplayName("turning right from WEST should give NORTH")
	public void testExecuteFromWest()   {
		
		// given
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.WEST);
		
		// when 
		new TurnRightAction(tondeuse).execute();
		
		// then
		assertSame(tondeuse.getOrientation(), Orientation.NORTH);
	}
	
	@Test()
	@DisplayName("turning right from SOUTH should give WEST")
	public void testExecuteFromSouth()   {
		
		// given
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.SOUTH);
		
		// when 
		new TurnRightAction(tondeuse).execute();
		
		// then
		assertSame(tondeuse.getOrientation(), Orientation.WEST);
	}
	
	@Test()
	@DisplayName("turning right from EAST should give SOUTH")
	public void testExecuteFromEast()   {
		
		// given
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.EAST);
		
		// when 
		new TurnRightAction(tondeuse).execute();
		
		// then
		assertSame(tondeuse.getOrientation(), Orientation.SOUTH);
	}

}
