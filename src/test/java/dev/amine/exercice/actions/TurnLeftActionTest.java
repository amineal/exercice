package dev.amine.exercice.actions;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import dev.amine.exercice.tondeuse.Tondeuse;
import dev.amine.exercice.tondeuse.actions.TurnLeftAction;
import dev.amine.exercice.tondeuse.models.Coordinate;
import dev.amine.exercice.tondeuse.models.Orientation;

public class TurnLeftActionTest {

	/**
	 * 	   NORTH
	 * <WEST * EAST>
	 *	   SOUTH
	 */
	
	private Coordinate maxCoords;
	private Coordinate startCoords;

	@BeforeEach
	public void setUp() {
		this.maxCoords = new Coordinate(3, 3);
		this.startCoords = new Coordinate(0, 0);
	}
	
	@Test()
	@DisplayName("turning left from NORTH should give WEST")
	public void testExecuteFromNorth()   {

		// given
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.NORTH);

		// when 
		new TurnLeftAction(tondeuse).execute();
		
		// then
		assertSame(tondeuse.getOrientation(), Orientation.WEST);
	}
	
	@Test()
	@DisplayName("turning left from WEST should give SOUTH")
	public void testExecuteFromWest()   {
		
		// given
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.WEST);
		
		// when 
		new TurnLeftAction(tondeuse).execute();
		
		// then
		assertSame(tondeuse.getOrientation(), Orientation.SOUTH);
	}
	
	@Test()
	@DisplayName("turning left from SOUTH should give EAST")
	public void testExecuteFromSouth()   {
		
		// given
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.SOUTH);
		
		// when 
		new TurnLeftAction(tondeuse).execute();
		
		// then
		assertSame(tondeuse.getOrientation(), Orientation.EAST);
	}
	
	@Test()
	@DisplayName("turning left from EAST should give NORTH")
	public void testExecuteFromEast()   {
		
		// given
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.EAST);
		
		// when 
		new TurnLeftAction(tondeuse).execute();
		
		// then
		assertSame(tondeuse.getOrientation(), Orientation.NORTH);
	}

}
