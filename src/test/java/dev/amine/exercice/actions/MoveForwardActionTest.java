package dev.amine.exercice.actions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import dev.amine.exercice.tondeuse.Tondeuse;
import dev.amine.exercice.tondeuse.actions.MoveForwardAction;
import dev.amine.exercice.tondeuse.models.Coordinate;
import dev.amine.exercice.tondeuse.models.Orientation;

public class MoveForwardActionTest {

	private Coordinate maxCoords;

	@BeforeEach
	public void setUp() {
		this.maxCoords = new Coordinate(3, 3);
	}
	
	@Test
	@DisplayName("move NORTH should increment Y axis")
	public void testExecutePossibleMoveNorth() {
		
		// given
		Coordinate startCoords = new Coordinate(0, 0);
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.NORTH);
		
		// when
		new MoveForwardAction(tondeuse).execute();
		
		// then
		assertEquals(tondeuse.getCoordinate(), new Coordinate(0, 1));
		
	}
	
	@Test
	@DisplayName("move EAST should increment X axis")
	public void testExecutePossibleEastMovment() {
		
		// given
		Coordinate startCoords = new Coordinate(0, 0);
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.EAST);
		
		// when
		new MoveForwardAction(tondeuse).execute();
		
		// then
		assertEquals(tondeuse.getCoordinate(), new Coordinate(1, 0));
		
	}
	
	@Test
	@DisplayName("move SOUTH should decrement Y axis")
	public void testExecutePossibleSouthMovment() {
		
		// given
		Coordinate startCoords = new Coordinate(2, 3);
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.SOUTH);
		
		// when
		new MoveForwardAction(tondeuse).execute();
		
		// then
		assertEquals(tondeuse.getCoordinate(), new Coordinate(2, 2));
		
	}
	
	@Test
	@DisplayName("move WEST should decrement X axis")
	public void testExecutePossibleWestMovment() {
		
		// given
		Coordinate startCoords = new Coordinate(2, 2);
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.WEST);
		
		// when
		new MoveForwardAction(tondeuse).execute();
		
		// then
		assertEquals(tondeuse.getCoordinate(), new Coordinate(1, 2));
		
	}
	
	@Test
	@DisplayName("move NORTH while max Y coord should not increment Y axis")
	public void testExecuteImpossibleNorthMovement()  {
				
		// given
		Coordinate startCoords = new Coordinate(2, 3);
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.NORTH);
		
		// when
		new MoveForwardAction(tondeuse).execute();
		
		// then
		assertEquals(tondeuse.getCoordinate(), new Coordinate(2, 3));
	}
	
	@Test
	@DisplayName("move WEST while min X coord should not decrement X axis")
	public void testExecuteImpossibleWestMovement()  {
		
		// given
		Coordinate startCoords = new Coordinate(0, 3);
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.WEST);
		
		// when
		new MoveForwardAction(tondeuse).execute();
		
		// then
		assertEquals(tondeuse.getCoordinate(), new Coordinate(0, 3));
	}
	
	@Test
	@DisplayName("move SOUTH while min Y coord should not decrement Y axis")
	public void testExecuteImpossibleSouthMovement()  {
		
		// given
		Coordinate startCoords = new Coordinate(3, 0);
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.SOUTH);
		
		// when
		new MoveForwardAction(tondeuse).execute();
		
		// then
		assertEquals(tondeuse.getCoordinate(), new Coordinate(3, 0));
	}
	
	@Test
	@DisplayName("move EAST while max X coord should not increment X axis")
	public void testExecuteImpossibleEastMovement()  {
		
		// given
		Coordinate startCoords = new Coordinate(3, 3);
		Tondeuse tondeuse = new Tondeuse(startCoords, maxCoords, Orientation.EAST);
		
		// when
		new MoveForwardAction(tondeuse).execute();
		
		// then
		assertEquals(tondeuse.getCoordinate(), new Coordinate(3, 3));
	}
}
